This procedure  will show how to use  Buildroot to build a
minimalist distribution targeted ARM architecture
 prerequisite packages on your host machine:
 in deb you can use apt install
 in centos/ fedora you can use dnf install / yum install
 
 binutils bzip2 unzip gcc make autoconf automake libtool
 patch git subversion bison flex gettext gawk texinfo gperf
 quilt screen
optional
 qt-devel ncurses-devel perl perl-ExtUtils* zlib-devel gcc-c++
